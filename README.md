# Prometey
### Recommended system requirements

```sh
php - ^8.0
mariadb - ^10
```


### Installation

After cloning the repository сreate `.env` file from `.env.example` and set needed configuration data.

```sh
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

Run the following commands
```sh
$ composer install
$ php artisan key:generate
$ php artisan config:cache
$ php artisan migrate
```

Postman-collection 

```sh
file TestWork 460.postman_collection.json
```
