<?php

namespace App\Http\Resources;

use App\Models\Phone;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PhonesResource
 * @package App\Http\Resources
 */
class PhonesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /* @var Phone|self $this */
        return [
            'id' => $this->id,
            'model' => $this->model,
            'number' => $this->number,
        ];
    }
}
