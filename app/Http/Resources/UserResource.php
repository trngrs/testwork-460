<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserResource
 * @package App\Http\Resources
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /* @var User|self $this */
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'phones' => PhonesResource::collection($this->phones)
        ];
    }
}
