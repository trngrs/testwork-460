<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthApi
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('authorization') && count(explode(' ', $request->header('authorization')))) {
            if ($user = User::where('remember_token', explode(' ', $request->header('authorization'))[1])->first()) {
                return $next($request);
            }
        }

        return response()->json([
            'success' => false,
        ], 401);
    }
}
