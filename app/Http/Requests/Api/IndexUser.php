<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiBaseRequest;

/**
 * Class CreateUser
 * @package App\Http\Requests\Api
 *
 * @property string $order_by
 * @property string $order_direction
 * @property string $find
 */
class IndexUser extends ApiBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'find' => ['string'],
            'order_direction' => ['string'],
            'order_by' => ['string'],
        ];
    }
}
