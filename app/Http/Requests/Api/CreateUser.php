<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiBaseRequest;

/**
 * Class CreateUser
 * @package App\Http\Requests\Api
 *
 * @property string $email
 * @property string $password
 */
class CreateUser extends ApiBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }
}
