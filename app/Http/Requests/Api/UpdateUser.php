<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiBaseRequest;

/**
 * Class UpdateUser
 * @package App\Http\Requests\Api
 *
 * @property array $phonies
 */
class UpdateUser extends ApiBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'number' => ['required'],
            'model' => ['required'],
        ];
    }
}
