<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiBaseRequest;

/**
 * Class UserLogin
 * @package App\Http\Requests\Api
 *
 * @property string $email
 * @property string $password
 */
class UserLogin extends ApiBaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }
}
