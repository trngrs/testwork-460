<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\UserLogin;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends ApiBaseController
{
    /**
     * @param UserLogin $request
     * @return JsonResponse
     */
    public function login(UserLogin $request)
    {
        $user = User::where('email', $request->email)->first();
        if (Hash::check($request->password, $user->password)) {
            $hash = base64_encode($user->emai . '-' . $user->id . '-' . date('YmdH:i:s:i'));
            $user->remember_token = $hash;
            $user->save();

            return response()->json([
                'accessToken' => $hash,
            ]);
        }

        return $this->invalidate(trans('messages.incorrect login or password'), false, 401);
    }
}
