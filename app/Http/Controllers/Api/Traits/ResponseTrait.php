<?php

namespace App\Http\Controllers\Api\Traits;

use Illuminate\Http\JsonResponse;
use JWTAuth;

/**
 * Trait ResponseTrait
 * @package App\Http\Controllers\Api\Traits
 */
trait ResponseTrait
{
    /**
     * @param $message
     * @param bool $key
     * @param int $status
     * @return JsonResponse
     */
    public function invalidate($message, $key = false, $status = JsonResponse::HTTP_UNPROCESSABLE_ENTITY): JsonResponse
    {
        $data = [
            'success' => false,
            'message' => $message,
        ];

        if ($key) {
            $data['errors'][$key] = $message;
        }

        return response()->json($data, $status);
    }

    /**
     * @param $message
     * @param $key
     * @return JsonResponse
     */
    public function invalidateField($message, $key): JsonResponse
    {
        $data['errors'][$key] = $message;

        return response()->json($data, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $keys_messages
     * @return JsonResponse
     */
    public function invalidateFields($keys_messages): JsonResponse
    {
        $data = [];

        foreach ($keys_messages as $key => $message) {
            $data['errors'][$key] = $message;
        }

        return response()->json($data, JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param $message
     * @param int $status
     * @return JsonResponse
     */
    public function success($message, $status = JsonResponse::HTTP_OK): JsonResponse
    {
        $data = [
            'success' => true,
            'message' => $message,
        ];

        return response()->json($data, $status);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function notFound(string $message = ''): JsonResponse
    {
        $message = $message ?: trans('messages.not found');

        return $this->invalidate($message, false, JsonResponse::HTTP_NOT_FOUND);
    }

    /**
     * @param string $message
     * @return JsonResponse
     */
    public function forbidden(string $message = ''): JsonResponse
    {
        $message = $message ?: trans('messages.access forbidden');

        return $this->invalidate($message, false, JsonResponse::HTTP_FORBIDDEN);
    }

    /**
     * Get the token array structure.
     *
     * @param string $token
     *
     * @return JsonResponse
     */
    protected function respondWithToken(string $token): JsonResponse
    {
        return response()->json([
            'accessToken' => $token,
            'tokenType' => 'bearer',
            'expiresIn' => JWTAuth::factory()->getTTL() * 60
        ]);
    }
}
