<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\Api\CreateUser;
use App\Http\Requests\Api\IndexUser;
use App\Http\Requests\Api\UpdateUser;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserController extends ApiBaseController
{

    protected UserRepositoryInterface $userRepository;

    /**
     * UserController constructor.
     * @param Request $request
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(Request $request, UserRepositoryInterface $userRepository)
    {
        parent::__construct($request);
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     * @param IndexUser $request
     * @return AnonymousResourceCollection
     */
    public function index(IndexUser $request)
    {
        return UserResource::collection($this->userRepository->index($request->getSanitized() , $this->page, $this->perPage));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateUser $request
     * @return UserResource
     */
    public function store(CreateUser $request)
    {
        return new UserResource($this->userRepository->create($request->getSanitized()));
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * @param User $user
     * @param UpdateUser $request
     * @return UserResource
     */
    public function update(UpdateUser $request, User $user)
    {
        return new UserResource($this->userRepository->update($user, $request->getSanitized()));
    }


}
