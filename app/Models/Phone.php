<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Phone
 *
 * @property-read int $id
 * @property string $model
 * @property string|null $number
 * @property-read Carbon|null $created_at
 * @property-read Carbon|null $updated_at
 *
 * @mixin Eloquent
 */
class Phone extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'model',
        'number',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'model' => 'string',
        'number' => 'string',
    ];

    /**
     * The validation rules.
     *
     * @var string[]
     */
    public array $rules = [
        'model' => 'required|string',
        'number' => 'nullable|string',
    ];
}
