<?php

namespace App\Repositories;

use App\Models\Phone;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function index(array $data, int $page, int $perPage): LengthAwarePaginator
    {
        $orderColumn = isset($data['order_by']) ? $data['order_by'] : 'id';
        $directionOrder = isset($data['order_direction']) ? $data['order_direction'] : 'asc';

        return User::where(function ($query) use ($data) {
                if (isset($data['find']) && $data['find'] != '') {
                    $query->where('name', 'like', '%' . $data['find'] . '%')
                        ->orWhere('email', 'like', '%' . $data['find'] . '%');
                }
            })
            ->orderBy($orderColumn, $directionOrder)
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): User
    {
        $data['name'] = rand(0000, 999999);
        $data['password'] = Hash::make($data['password']);
        $user = new User($data);
        $user->save();

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function update(User $user, array $data): User
    {
        $phone = new Phone($data);
        $phone->save();

        $user->phones()->syncWithoutDetaching([$phone->id]);

        return $user->refresh();
    }
}
