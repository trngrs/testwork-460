<?php

namespace App\Repositories\Contracts;

use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface UserRepositoryInterface
{
    /**
     * @param array $data
     * @param int $page
     * @param int $perPage
     * @return LengthAwarePaginator
     */
    public function index(array $data, int $page, int $perPage): LengthAwarePaginator;

    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User;

    /**
     * @param User $user
     * @param array $data
     * @return User
     */
    function update(User $user, array $data): User;
}
